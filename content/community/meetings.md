+++
title = "Meetings"
[menu.main]
parent = "community"
identifier = "meetings"
weight = 2
+++

## User Meetings and Conferences

* [User Meeting 2015](http://users.dune-project.org/projects/dune-user-meeting-2015) in Heidelberg
* [PDESoft 2014](http://pdesoft.uni-hd.de/) in Heidelberg
* [User Meeting 2013](http://users.dune-project.org/projects/user-meeting-2013/wiki/Dune_User_Meeting_2013) in Aachen
* [PDESoft 2012](http://pdesoft2012.uni-muenster.de/) in Münster
* [User Meeting 2012](http://users.dune-project.org/projects/user-meeting-2012/wiki) in Münster
* [User Meeting 2010](http://users.dune-project.org/projects/user-meeting-2010/wiki) in Stuttgart

## Developer Meetings

* [Developer Meeting 2015](http://users.dune-project.org/projects/dune-developer-meeting-2015) in Heidelberg
* [Developer Meeting 2014](http://users.dune-project.org/projects/dev-meeting-2014/wiki/Wiki) in Berlin
  ([Protocol](http://users.dune-project.org/projects/dev-meeting-2014/wiki/Protocol))
* [Developer Meeting 2013](http://users.dune-project.org/projects/dev-meeting-2013/wiki/Wiki) in Aachen
* [Developer Meeting 2012](http://users.dune-project.org/projects/dev-meeting-2012/wiki) in Münster
* [Developer Meeting 2010](http://users.dune-project.org/projects/dune-dev-meeting-201/wiki) in Münster
* [Developer Meeting 2009](2009-11-devmeeting)
* [Developer Meeting 2008](2008-02-27-devmeeting) in Berlin
* [Developer Meeting oct. 2005](2005-10-05-devmeeting) in Freiburg (aka Hüttenworkshop)
* [Developer Meeting july 2005](2005-07-11-devmeeting)
* [Developer Meeting jan. 2005](2005-01-21-devmeeting) in Heidelberg
* [Developer Meeting 2004](2004-10-07-devmeeting)

## Special Topic Meetings

* [Dune-FEM Meetings](http://users.dune-project.org/projects/dune-fem-meeting-2015/wiki)
* [GlobalFunctions 2013](http://users.dune-project.org/projects/globalfunctions2013/wiki) in Münster
