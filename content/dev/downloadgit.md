+++
title = "Download via Git"
[menu.main]
parent = "dev"
weight = 2
+++
## Download unstable DUNE modules using Git

DUNE consists of several separate modules. Depending on the application you are writing you will need a certain set of the modules.

DUNE development happens using Git. If you want access to the current development version, you can clone the Git repositories of the DUNE modules. All modules are dependent on the base module dune-common and hence it is necessary to have dune-common in all cases.

The central place for development is our GitLab instance at http://gitlab.dune-project.org. It hosts the main repositories for the core modules, its bug trackers and is the preferred place for contributions. For visibility reasons, the repositories for the core modules are mirrored on [GitHub](https://github.com/dune-project).

There are some guides and pointers to Git tutorials on the user [wiki](http://users.dune-project.org/projects/main-wiki/wiki/Guides_dune_core_modules#Dune-and-Git), as well as some DUNE-specific information regarding recommended hooks and our repository layout. If you want to contribute back to DUNE (great - we are looking forward to it!), please make sure to read about our [whitespace policy](http://users.dune-project.org/projects/main-wiki/wiki/Whitespace_hook) and about how to [get those patches](http://users.dune-project.org/projects/main-wiki/wiki/Contributing_patches) to us.

### Core Modules

* [dune-common](http://www.dune-project.org/doc/doxygen/dune-common-html/modules.html) (basic classes)<br>
   `git clone https://gitlab.dune-project.org/core/dune-common.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-common))
* [dune-geometry](http://www.dune-project.org/doc/doxygen/dune-geometry-html/modules.html) (element geometries)<br>
  `git clone https://gitlab.dune-project.org/core/dune-geometry.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-geometry))
* [dune-grid](http://www.dune-project.org/doc/doxygen/dune-grid-html/modules.html) (abstract grid/mesh interface)<br>
  `git clone https://gitlab.dune-project.org/core/dune-grid.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-grid))
* [dune-istl](http://www.dune-project.org/doc/doxygen/dune-istl-html/modules.html) (Iterative Solver Template Library)<br>
  `git clone https://gitlab.dune-project.org/core/dune-istl.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-istl))
* [dune-localfunctions](http://www.dune-project.org/doc/doxygen/dune-localfunctions-html/modules.html) (Interface for the local basis and layout of the degrees of freedom))<br>
  `git clone https://gitlab.dune-project.org/core/dune-localfunctions.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-localfunctions))

### Tutorial Modules

* [dune-grid-howto](/doc/tutorials/grid-howto.pdf) (tutorial on the grid/mesh interface)<br>
  `git clone https://gitlab.dune-project.org/core/dune-grid-howto.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-grid-howto))
