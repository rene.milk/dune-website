+++
title = "Installing ALUGrid for DUNE"
+++

### Installing ALUGrid for DUNE

#### Deprecation Notice
The stand-alone ALUGrid package is no longer maintained and its DUNE bindings are deprecated.
Users are encouraged to migrate to the new DUNE-ALUGrid module which contains both, the ALUGrid implementation and their DUNE interface. This also simplifies the installation process the DUNE build system can be used to configure [DUNE-ALUGrid](http://users.dune-project.org/projects/dune-alugrid). For further information, please have a look at the DUNE-ALUGrid project page.

#### Dependencies
There are no mandatory packages for ALUGrid.
For optional packages (which are required for parallel use) see the README file of ALUGrid.

#### Compilation and Installation
1. Download the latest version of [ALUGrid](http://dune.mathematik.uni-freiburg.de/downloads/ALUGrid-1.52.tar.gz).
2. Unpack it in a directory
  `tar xzf ALUGrid-1.x.tar.gz`
  and move to that directory
  `cd ALUGrid-1.x`
  and read the README file first.
3. Configure ALUGrid, e. g.
  `./configure --prefix=/directory/to/install/ALUGrid/to` for the sequential use of ALUGrid.

  With `--prefix`the absolute path of the directory we install to has to be given. If no `--prefix` given, ALUGrid is installed into the directory where it was unpacked.

  With the variables CC and CXX you may tell configure the C and C++ compiler and linker to use. These have to be the same compilers and linkers you will use to compile DUNE!!

  **Warning:** DUNE automatically passes `-std=c++0x` to the C++ compiler (if supported). It is highly recommended that you also add this flag to the CXXFLAGS when configuring ALUGrid. Due to a different ABI in C++0x (C++11) mode, gcc version 4.7.0 and 4.7.1 will even produce runtime faults if this flag is omitted (see the [GCC 4.7 Release Notes](http://gcc.gnu.org/gcc-4.7/changes.html)).

  If you want to use the parallel version of ALUGrid, please read the README file in the ALUGrid distribution. For further information and additional options see
  ```
  ./configure --help
  ```

4. Compile ALUGrid with
  ```
  make
  ```
5. Install ALUGrid with
  ```
  make install
  ```
6. Add
  ```
  --with-alugrid=/directory/where/alugrid/is/installed
  ```
  as an option to configure for dune-grid.
