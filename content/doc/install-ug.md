+++
title = "Installing UG"
+++

### Installing UG for DUNE
UG is a software tool for the numerical solution of partial differential equations on unstructured meshes in two and three space dimensions using multigrid methods. UG runs both sequentially and in parallel. DUNE offers an abstraction to the UG grid manager through the UGGrid class.

#### Getting UG

UG is free software, available under the LGPLv2+. You can download the source from the [UG homepage](http://www.iwr.uni-heidelberg.de/frame/iwrwikiequipment/software/ug).

The appropriate UG version for you depends on which version of DUNE you want to use.

* The git master of dune-grid works with UG version 3.12.0 or later
* The 2.4 release works with UG version 3.12.0.
* The 2.3 release works with UG version 3.10.0.

#### Compilation and Installation
UG has a standard AutoTools build system. However, to use it with dune-grid you need to set a few non-standard options.

1. If you are building UG from a tarball: Unpack the tarball, and enter the main directory
2. If you are building UG from the git sources: Enter the main directory and call
    `autoreconf -is`
3. Build the UG make system
    ```
     ./configure --prefix=my_favourite_ug_installation_path --enable-dune CC=g++
    ```

    * --prefix determines the absolute path of the directory we install to.
    * --enable-dune enables special features needed by Dune.
    * CC=g++ tells the build system to compile everything as C++, even though it is technically C.

    If your UG is version 3.12.0 or newer, you can drop the CC=g++ option.

4. Compile UG with
    ```
    make
    ```
5. Install UG with
    ```
    make install
    ```
6. If you are building dune-grid with the CMake build system (recommended), add
    ```
    CMAKE_FLAGS+=" -DCMAKE_PREFIX_PATH=/my/ug/installation/path "
    ```
    as a CMake option for dune-grid.  If you are building using the AutoTools
    build system, add
    ```
    --with-ug=my_favourite_ug_installation_path
    ```
    as an option to configure for dune-grid.
