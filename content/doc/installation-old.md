+++
title = "Old Installation Notes"
+++

### Old Installation Notes
These installation notes for Autotools are outdated. Beginning with DUNE 2.4 CMake is the official build-system. Use the [current instructions](/doc/installation).

#### Dependencies
In order to build DUNE you need at least the following software:

* a standard compliant C++ compiler, tested are g++ (>= 4.4) and Clang (>=3.4). Recent versions of ICC (>= 15) should work, older versions like 14.0.3 needs patching of system headers and is discouraged. ([More](http://users.dune-project.org/projects/main-wiki/wiki/Compilers))
* pkg-config

The following software is recommend but optional:

* MPI (either OpenMPI, lam, or mpich suffice)

When building the development version of DUNE you will also need

* automake (>= 1.9)
* autoconf (>= 2.62)
* libtool (>= 2.2)

This will provide you with the core DUNE features.

Some DUNE modules might support further software. Using this software is optional. The dune-grid module for example supports different external grid managers like Alberta or UG; these have to be downloaded separately. For a list of supported contrib software packages and their installation see the notes on [Installation External Libraries](/doc/external-libraries).

#### Installing the Core DUNE Modules
Suppose you have downloaded all DUNE modules of interest to your computer and extracted then in one common directory. See the [download](/download) section for a list of available modules.
To compile the modules DUNE has to check several components of your system and whether prerequisites within the modules are met. For the ease of users we have designed a custom build system on top of the automake tools. Run

`./dune-common/bin/dunecontrol all`

to commence those tests and build all modules you have downloaded. Don't worry about messages telling you that libraries are missing: they are only needed for grid-self-checks we need for developing.

You can customize the build to your specific needs by using an options file (see below)

`./dune-common/bin/dunecontrol --opts=/path_to/file.opts`

If you did not tell dunecontrol to install with an options file you need to run

`./dune-common/bin/dunecontrol make install`

to install DUNE (you may need root-permissions for the install part depending on the prefix set).
A more comprehensive introduction to the build system can be found in the [Dune Build System Howto](/doc/buildsystem/builsystem.pdf).

#### Building a Specific DUNE Module (and its dependent modules)
You can instruct dunecontrol to build only a certain dune module, using the `--only=module_name>` switch. Runnning dunecontrol script
```
./dune-common/bin/dunecontrol --only=<module_name> all
```

where `<module_name>` is the name of that particular module given in the dune.module file, will build only the module `<module_name>`.
If you want to build a module and the modules it depends on, you must run:

```
./dune-common/bin/dunecontrol --module=module_name all
```

Read [Maintaining new DUNE Modules and Applications ](/modules/dune-module) section for more details.

#### Passing Options to the Build Process
Using the dunecontrol script the following atomic commands can be executed:

* autogen (runs autogen in each module, only needed when downloaded via Git)
* configure (runs the configure tests for each module
* exec (executes a command in each module directory)
* make (runs make for each module)
* update (updates the Git version)

The composite command all simply runs autogen, configure and make for each module.
As it is often not convenient (and for the target all impossible) to specify the options for each command as parameters after the call, one can pass the options via file specified by the `--opts=<file>` option. For each atomic command one specify the options via a ine
```
<COMMAND_UPPERCASE>_FLAGS=<flags> # e.g.: MAKE_FLAGS=install
```

The available options for make are the natural ones. The configure commands available can be found by issuing
```
dunecontrol --only=dune-common configure --help
```

and for autogen by
```
dunecontrol --only=dune-common autogen --help
```
(In the Git version this has to be called after running autogen.)
An example of an options file is
```
# use a special compiler (g++ version 3.4) and install to a custom
# directory, default is /usr/local/bin
CONFIGURE_FLAGS="CXX=g++-3.4 --prefix='/tmp/HuHu'"
# Set the default target of make to install. Now the call above will
# not just build the DUNE modules but also install it
MAKE_FLAGS=install
# The default versions of automake and autogen are not sufficient
# therefore we need to specify what versions to use
AUTOGEN_FLAGS="--ac=2.59 --am=1.9"
```
On some platforms special care might have to be taken to make things compile, e.g. this is the case for IBM's Blue Gene System P. If you want to run DUNE there please read the [instructions](/doc/installation-bluegenep) of Markus and save yourself a lot of time.

#### Creating your own DUNE Project Module
You can create your own dune project module by using the duneproject script available in dune-common/bin directory. Running the script will create a directory with supporting files (configure.ac, Makefile.am etc.) and a sample .cc file. After creating the module you can build this as explained above under "Building a specific DUNE module".

The [DUNE Build System Howto](/doc/buildsystem/buildsystem.pdf) will also give you an excellent introduction to the build system and how to create new modules/projects your own.
