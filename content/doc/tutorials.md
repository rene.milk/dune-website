+++
title = "Tutorials"
[menu.main]
parent = "docs"
weight = 3
+++
### Using DUNE

If you are new to Dune, check out the text on
[how to get started](http://www.math.tu-dresden.de/~osander/research/sander-getting-started-with-dune.pdf).


#### Installation and the build system
+ [Installation Notes](/doc/installation) for the DUNE core modules.
+ An FAQ on the new [CMake-based build system](/doc/buildsystem/cmakefaq.pdf).
+ A tutorial on the [AutoTools build system](/doc/buildsystem/buildsystem.pdf).
**Note:** The AutoTools build system was available up until version 2.4. All versions including 2.4 and later should be built with the cmake built system.

#### dune-grid
+ A tutorial on the Dune grid interface. Download the [pdf](/doc/tutorials/grid-howto.pdf).
+ The [view concept](/doc/view-concept) is the fundamental idea of the grid interface
+ A description of [Refinement](/doc/tutorials/refinement.pdf), a system for local and temporary refinements.

#### dune-istl
+ ISTL [tutorial](/doc/tutorials/istl.pdf)
+ Description of the [parallel communication interface](/doc/tutorials/communication.pdf)

#### dune-localfunctions
+ dune-localfunctions [manual](/doc/tutorials/dune-localfunctions-manual.pdf)


#### dune-FEM
+ dune-FEM [howto](/dune-fem/files/dune-fem-howto-1.3.0.pdf)

#### dune-PDELab
+ dune-PDELab [howto](/dune-pdelab/files/pdelab-howto-2.0.0.pdf)
