+++
group = "core"
title = "Core Modules"
[menu.main]
parent = "modules"
weight = 1
+++

The *Dune core modules* build the stable basis of Dune. They follow a
consistent release cycle and have high requirements regarding
stability and backwards compatibility. These modules build the
foundation for higher-level components like [discretization modules]({{< relref "disc.md" >}}).
