+++
group = "disc"
title = "Discretization Modules"
[menu.main]
parent = "modules"
weight = 3
+++
Dune has a modular structure and it is possible to write your own
discretization schemes based directly on the core modules. But there
are also a number of modules maintained by the DUNE developers which
add different discretization methods to the
[Dune core modules](/groups/core).

* [dune-FEM](/modules/dune-fem)
is a discretization module providing an implementation of mathematical abstractions to solve PDEs on parallel computers including local grid adaptivity, dynamic load balancing, and higher order discretization schemes.
[[Homepage](http://dune.mathematik.uni-freiburg.de/), [Class Documentation](http://dune.mathematik.uni-freiburg.de/doc/html-current/), and [tutorial](http://dune.mathematik.uni-freiburg.de/doc/dune-fem-howto-current.pdf)]

* dune-fufem
is a discretization module that emphasizes simplicity and ease of use over flexibility and performance. It is maintained by [Carsten Gräser](http://page.mi.fu-berlin.de/graeser/), [Uli Sack](http://numerik.mi.fu-berlin.de/Kontakt/uli_sack.php/), and [Oliver Sander](http://page.mi.fu-berlin.de/sander/).

* [dune-PDELab](/modules/dune-pdelab)
is a new generalized discretization module for a wide range of discretization methods. It allows rapid prototyping for implementing discretizations and solvers for systems of PDEs based on DUNE.
[[Homepage](http://www.dune-project.org/pdelab), [Class Documentation](http://www.dune-project.org/doc-pdelab-2.0.0/doxygen/html/), and [How to](/dune-pdelab/files/pdelab-howto-2.0.0.pdf)]


Older modules which are not maintained anymore

* dune-disc (shapefunctions, discretization schemes, ...)
  svn checkout https://svn.dune-project.org/svn/dune-disc/trunk dune-disc [browse svn](http://svn.dune-project.org/websvn/listing.php?repname=dune-disc&path=%2Ftrunk%2F&rev=0&sc=0)
