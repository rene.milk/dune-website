+++
group = "external"
title = "External Modules"
[menu.main]
parent = "modules"
weight = 4
+++

### External Modules

DUNE has a modular structure and it is possible to write your own DUNE modules. This is a list of a few externally maintained extension modules that we know about. There are many more DUNE modules around maintained by different groups. Some are hosted on the [project page](http://users.dune-project.org/projects) of our user Wiki and if you are looking for something not mentioned then it might help to send an email to the DUNE list to describing the feature you are looking for.

### Highlevel Applications & Simulation Frameworks

These are actually not Dune modules, but general software packages that use Dune in one way or another for infrastructure.

* [DuMu<sup>x</sup>](http://www.dumux.org/) is a DUNE based simulator for flow and transport processes in porous media.
* [Kaskade 7](http://www.zib.de/en/numerik/software/kaskade-7.html) uses Dune for the grid and linear algebra infrastructure.
* The [Open Porous Media Simulator (OPM)](http://opm-project.org/) initiative has the goal to develop a DUNE based simulation suite that is capable of modeling industrially and scientifically relevant flow and transport processes in porous media.
* [BEM++](http://www.bempp.org/) is an open source C++/Python library for boundary element methods.

### Grid Managers

* [dune-alugrid](http://users.dune-project.org/projects/dune-alugrid) is the implementation of the DUNE grid interface providing unstructured simplicial and cube grids.
* [dune-foamgrid](/modules/dune-foamgrid/) is a 2d-in-nd grid manager supporting non-manifold topologies.
* [dune-multidomaingrid](http://github.com/smuething/dune-multidomaingrid) is a meta grid that allows to divide a given grid into separate subdomains. These subdomains can be treated as grids in their own right. dune-multidomaingrid is maintained by [Steffen Müthing](mailto:steffen.muething@iwr.uni-heidelberg.de).
* [dune-spgrid](http://dune.mathematik.uni-freiburg.de/grids/dune-spgrid) provides a structured, parallel grid (SPGrid).
* [dune-subgrid](http://numerik.mi.fu-berlin.de/dune-subgrid/index.php) allows you to mark a subset of the elements of a given grid. This subset can then be treated as a grid hierarchy in its own right.

### Others

* The [dune-functions](/modules/dune-functions) module provides an abstraction layer for global finite element functions. Its two main concepts are functions implemented as callable objects and bases of finite element spaces.
* The [dune-grid-glue](/modules/dune-grid-glue) module allows to compute overlapping and nonoverlapping couplings of DUNE grids. This is the basis for most domain decomposition algorithms.
* The [dune-mc](/modules/dune-mc/) module provides a topology preserving implementation of the marching cubes and marching simplex algorithm. This module can be used to implement cut-cell algorithms on top of the Dune interface.
* [dune-solvers](http://numerik.mi.fu-berlin.de/dune/dune-solvers) contains a variety of algebraic solvers structured in a hierarchy with dynamical polymorphism.
