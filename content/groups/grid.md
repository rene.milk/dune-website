+++
group = "grid"
title = "Grid Modules"
[menu.main]
parent = "modules"
weight = 2
+++
### Grid managers

The dune-grid module already includes some grid managers. Some are DUNE internal but most require some additional packages. Look at an [overview](http://users.dune-project.org/projects/main-wiki/wiki/Grid-Manager_Features) of the different grid manager provided and there feature. In addition to those provided directly with dune-grid, there are further grid managers with different sets of features available - some of these might require the installation of some additional library. Not all the grid managers listed here are available for download but feel free to contact the maintainer to ask for a tar ball.

* [dune-foamgrid](/modules/dune-foamgrid) is a 2d-in-nd grid manager supporting non-manifold topologies.
* [dune-multidomaingrid](http://github.com/smuething/dune-multidomaingrid) is a meta grid that allows to divide a given grid into separate subdomains. These subdomains can be treated as grids in their own right. dune-multidomaingrid is maintained by [Steffen Müthing](mailto:steffen.muething@iwr.uni-heidelberg.de).
* [dune-prismgrid](http://www.mathematik.uni-freiburg.de/IAM/homepages/gersbach/prismgrid/) is a grid wrapper for a DUNE grid implementation, adding an additional dimension to the grid using generic prismatic elements. This grid is maintained by [Christoph Gersbacher](http://www.mathematik.uni-freiburg.de/IAM/homepages/gersbach/index.html).
* [dune-subgrid](http://numerik.mi.fu-berlin.de/dune-subgrid/index.php) allows you to mark a subset of the elements of a given grid. This subset can then be treated as a grid hierarchy in its own right.
