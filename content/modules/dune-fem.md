+++
module = "dune-fem"
group = "disc"
requires = ["dune-common", "dune-grid"]
suggests = ["dune-istl"]
title = "dune-fem"
short = "A discretization module providing an implementation of mathematical abstractions to solve PDEs on parallel computers including local grid adaptivity, dynamic load balancing, and higher order discretization schemes."
+++

### General

DUNE-FEM is a discretization module based on [DUNE](/about/home). It provides interfaces for:

- Discrete function spaces and functions
- Linear operators and inverse linear operators
- Methods for solving non-linear equations and time dependent problems
- MPI and multithread based parallelization
- Additional grid views e.g. for removing parts of the grid (filtering) or replacing geometry information

In addition DUNE-FEM provides many auxiliary classes for

+ Flexible parameter input
+ Data output including checkpointing
+ Computation of eoc tables, timers for subroutines, and much more...

Efficient strategies for parallelization, adaptivity, and load balancing is a central aspect in the construction of DUNE-FEM. Both are handled by central classes and require very little input from the user. To increase efficiency we try to use caching as much as possible, e.g., for communication patterns in parallel computations and of base function sets in quadrature points on the reference elements. All this is done transparently from the user but can be turned off anywhere in the code.

To measure efficiency we have used a Discontinuous Galerkin method for the compressible Navier-Stokes equation to compute both parallel scale up and FLOP performance. The code showed a parallel efficiency close to one up to 16K processors and we measured 5 GFLOPs on an Intel Core i7 QM 720 @ 1.6 GHz which is about 41 percent of the FLOPs we measured for LINPACK on the same machine.

### Main Features

DUNE-FEM does not only provide interfaces but also comes with realizations of these interfaces:

+ Discrete function spaces:
  - Arbitrary order spaces with Lagrange basis functions and orthonormal basis functions
  - Arbitrary order Lagrange spaces
  - p-adptive DG and Lagrange spaces
  - spaces for reduced basis methods
+ Discrete function implementations:
  - Discrete function spaces especially build to allow for very efficient grid refinement and coarsening
  - Discrete function based on the block vectors provided by dune-istl
  - Different approaches to extend scalar discrete function spaces to product spaces
+ Inverse linear operators:
  - DUNE-FEM provides some build in iterative solvers, e.g., cg, gmres, and bicgstab
  - There are bindings for dune-istl, umfpack, and petsc
+ Time step methods:
  - SSP Runge-Kutta methods (explicit, implicit, and IMEX)
  - Multistep methods

### Applications
DUNE-FEM has been used for a wide range of applications:
+ Higher order methods for conservation laws (both finite-volume and discontinuous Galerkin)
+ Convection dominated convection-diffusion equations (mostly Discontinuous Galerkin)
+ Higher order methods for parabolic and elliptic problems (both conforming and Discontinuous Galerkin)
+ Hamilton Jacobi equations (different methods used)
+ Two-phase flow in porous media (finite-volume and Discontinuous Galerkin)
+ Reduced basis methods

Most methods have been tested for both problems formulated in Euclidean space and on surfaces including moving surfaces and also moving domains.

### Gallery

To get a brief overview of what has already been done with DUNE-FEM, visit our [gallery](/dune-fem/gallery) or have a look at our [applications page](http://dune.mathematik.uni-freiburg.de/application.html).

### Documentation

The module dune-fem-howto contains a tutorial and example applications that illustrate what DUNE-FEM can do. It can be downloaded from the [DUNE-FEM](http://dune.mathematik.uni-freiburg.de/index.html) homepage.

There is one main publication describing some of the underlying principals and giving some examples and we ask all users of DUNE-FEM to cite in publications using DUNE-FEM:

A. Dedner, R. Klöfkorn, M. Nolte, M. Ohlberger A generic interface for parallel and adaptive scientific computing: Abstraction principles and the DUNE-FEM module. Computing Vol. 90, No. 3, pp. 165--196, 2011 [preprint](http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/postscript/dkno_dunefempreprint.pdf).

#### Other Resources:

+ [Online Documentation for DUNE-FEM 1.4](http://dune.mathematik.uni-freiburg.de/doc/html-current/index.html) (compatible with the 2.3 core modules).
+ Step by step introduction to dune-fem using the [DUNE-FEM-Howto](http://dune.mathematik.uni-freiburg.de/download.html).
+ [Further Publications](http://dune.mathematik.uni-freiburg.de/publications.html)


### Download

DUNE-FEM is very stable and we try to make changes in such a way that they do not break existing code. Nevertheless we suggest to use the tar balls provided on the homepage. We provide new tar balls at least for every new release of the core modules - but note that the release is not always going to be compatible with the trunk of the core modules. Please contact the DUNE-FEM team if you want access to the trunk version. More information can be found on the [DUNE-FEM homepage](http://dune.mathematik.uni-freiburg.de/index.html).

### Support

DUNE-FEM provides its own mailing lists dune-fem@dune-project.org and please direct inquiries there. Go to this website to [subscribe](http://lists.dune-project.org/mailman/listinfo/dune-fem).
