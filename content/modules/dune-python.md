+++
# The name of the module.
module = "dune-python"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = "extension"

# List of modules that this module requires
requires = ["dune-common"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[Dominic Kempf](mailto:dominic.kempf@iwr.uni-heidelberg.de)"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/quality/dune-python.git"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "A buildsystem extension to distribute python code with the Dune CMake build system"
+++

Although Dune is a C++ framework, Python is an interesting choice for many parts
of the simulation workflow. Distributing software based on Dune, which uses both
C++ and Python, you will inevitably face the problem of Python and Dune thinking
differently about how to configure, install and ship things.

dune-python offers a solution to that problem by providing a CMake build system
extension, that offers an easy to use interface to add python packages and scripts
to a Dune module. At configure time, dune-python will create a [virtualenv](https://virtualenv.pypa.io/en/stable/)
in your build directory and install python packages and their requirements into
that environment. This way you can keep the requirements on the python environment
on the system minimal.

For more detailed information about dune-python, check its [documentation](/sphinx/dune-python).
