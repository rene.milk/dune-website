+++
date = "2007-11-26"
title = "Reading Facilities for the Star-CD File Format Added"
+++

Bernd Flemisch has kindly donated code that allows to read grids in the Star-CD format. It works for all grid implementations that have the *grid construction interface* of UGGrid.

[Update this interface evlved into the generic [grid factory interface](/doxygen/master/classDune_1_1GridFactory.html)]

[Update the StarCD support was removed in favor of open-source alternatives]
