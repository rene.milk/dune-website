+++
date = "2007-12-04"
title = "RSS News Feed"
+++

From today on the DUNE News are also available via an RSS 2.0 feed [![RSS 2.0 Feed](http://www.dune-project.org/pics/rss20.png)](http://www.dune-project.org/news/rss.xml). Together with the RSS Feed we also added functionality to keep the [dune-announces](http://lists.dune-project.org/mailman/listinfo/dune-announce) mailing list synced with the news section on the web site.
