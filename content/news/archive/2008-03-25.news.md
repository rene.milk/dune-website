+++
date = "2008-03-25"
title = "UG patches available"
+++

The patches to the vanilla UG which you need in order to use a version of UGGrid newer than 1.0 are finally available for download. Have a look at the [installation instructions](http://www.dune-project.org/external_libraries/install_ug.html) for details.
