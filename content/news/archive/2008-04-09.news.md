+++
date = "2008-04-09"
title = "Release of DUNE 1.1"
+++

We are pleased to announce the release of Version 1.1 of the "Distributed and Unified Numerics Environment" (DUNE) [(download)](/releases/).

DUNE is free software licensed under the GPL (version 2) with a so-called "runtime exception". This licence is similar to the one under which the libstdc++ libraries are distributed. Thus it is possible to use DUNE even in proprietary software.

The changes to from version 1.0 to version 1.1 are listed in the [release notes](/releases/1.1.0/). For detailed information a file containing the differences of the source file from each core module is available in the [download directory](/releases/1.1.0/).
