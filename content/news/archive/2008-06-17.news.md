+++
date = "2008-06-17"
title = "New list dune-disc-commits"
+++

There is a new mailing list `dune-disc-commit` which all commit messages for the dune-disc package will be sent over.

*Update:* `dune-disc` is not developed any further
