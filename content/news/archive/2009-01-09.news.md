+++
date = "2009-01-09"
title = "New ALUGrid Version"
+++

ALUGrid version 1.13 has been released. The new C++ standard is now completely supported. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
