+++
date = "2009-02-13"
title = "New ALUGrid Version"
+++

ALUGrid version 1.14 has been released. The code compiles now also on Mac OS X systems. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
