+++
date = "2009-02-18"
title = "Schedule for DUNE Course March 25-27, 2009 is online"
+++

The schedule of the DUNE Course at IWR, University Heidelberg, Germany, is online. Please see the course homepage at <http://conan.iwr.uni-heidelberg.de/dune-workshop/> for more details.

We are pleased to annouce that the second day of the DUNE course will be contributed by the DUNE developers at IAM Freiburg.

By participating in this course scientists have the opportunity to get a hands-on introduction to the DUNE framework. Main focus is to give a detailed introduction to the DUNE core modules: the grid interface including IO methods with its numerous grid implementations and a brand new module supporting easy and natural discretizations. In the exercises elliptic and hyperbolic model problems will be solved with various methods.

In addition, a course on advanced C++ programming will be held before the DUNE workshop on March 23-24, 2009.
