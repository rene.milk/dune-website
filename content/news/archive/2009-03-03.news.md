+++
date = "2009-03-03"
title = "First Release Candidate of Dune 1.2 Available"
+++

The first release candidate of the upcoming Dune 1.2 release has been made available for [download](/releases/). We expect these modules to be quite stable already. Please test them and report any problems.
