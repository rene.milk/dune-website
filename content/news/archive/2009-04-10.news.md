+++
date = "2009-04-10"
title = "New numbering in the reference elements introduced"
+++

This week we introduced a new numbering of the subentities of the reference elements. The reason is that the new numbering is more consistent and generalizes more easily to higher dimensions. This change may affect your code in subtle ways if you depend on a particular numbering. We have assembled a [page](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___grid_dune_r_e_to_generic_r_e.html) with the technical details.
