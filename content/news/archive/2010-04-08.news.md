+++
date = "2010-04-08"
title = "DUNE 2.0 beta1 available"
+++

Today we prepared the first beta for the upcoming DUNE 2.0 release. Everything took a bit longer than expected, which was mainly due to the large number of last-minute-features we decided on in Berlin last November. But the long freeze is approaching an end.

The release will include the three core module `dune-common`, `dune-grid`, and `dune-istl`; and the howtos `dune-grid-howto` and `dune-grid-dev-howto`. Together with these five packages we will release the new package `dune-localfunctions`, which brings a new interface for finite element shape functions to Dune.

Follow the [download link](/releases/2.0.0/), grab the packages, test them!

*Updated:* refer to final release
