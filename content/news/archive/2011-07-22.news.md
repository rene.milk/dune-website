+++
date = "2011-07-22"
title = "DUNE-FEM 1.2.0 Released"
+++

The [Dune-Fem module](http://dune.mathematik.uni-freiburg.de/) is based on the Dune-Grid interface library, extending the grid interface by a number of higher order discretization algorithms for solving non-linear systems of partial differential equations. This includes for example the automatic handling of the degrees of freedom (DoF) on locally adapted and parallel grids - including dynamic load balancing. Furthermore, a tutorial named Dune-Fem-Howto containing several examples of **how to use Dune-Fem** is available on the Dune-Fem homepage. For more details, have a look at the [Dune-Fem page](http://dune.mathematik.uni-freiburg.de/).
