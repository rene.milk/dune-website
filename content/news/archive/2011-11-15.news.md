+++
date = "2011-11-15"
title = "gdb pretty-printers for easier debugging"
+++

An initial set of gdb pretty-printers for Dune data structures has been released. Pretty printers transform the gdb output of data structures to make it easier to read. Currently the classes FieldVector, FieldMatrix, and BitSetVector are supported. See the [project page](http://users.dune-project.org/projects/dune-gdb-pretty-printers) for a brief description. Everybody using gdb (or its graphical front-ends) for debugging is invited to try and give feedback.

Thanks go to Jan-Hendrik Peters, who did all the work.
