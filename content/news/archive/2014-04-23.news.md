+++
date = "2014-04-23"
title = "Dune-Fem 1.4.0 Released"
+++

The developers of Dune-Fem are pleased to announce the release of version 1.4.0 of their discretization module. This version is compatable with the 2.3 release series of the Dune core modules.

Besides the usual ton of bug fixes and compatiblity with the current release of the Dune core modules, the interfaces of Dune-Fem have been further clarified. Discrete function spaces have been overhauled to simplify the integration of dune-localfunctions, the assembly of linear operators has seen a major revision, and initial support for PETSc has been added.

For further information and downloads see the [Dune-Fem homepage](http://dune.mathematik.uni-freiburg.de/).
