+++
date = "2014-11-25"
title = "CMake is default build system"
+++

Last weekend we switched our default build system. Instead of Autotools, CMake is now the default.

We hope to lower the barrier to make changes to the build system, reduce configure and compile times, get less cluttered output, and use a more modern system that is still actively developed.

-   Support for Autotools is deprecated and will be removed after Dune 2.4.
-   Pass --no-cmake to dunecontrol to force the use of Autotools.
-   If no build directory is given make files, libraries and executables are in &lt;module-dir&gt;/build-cmake/ .
-   Our CMake-related documentation can be found in the Dune user wiki: <http://users.dune-project.org/projects/main-wiki/wiki/Using_cmake_to_build_your_DUNE_project>
-   A quick guide for the switch: <http://users.dune-project.org/projects/main-wiki/wiki/Switching_to_CMake>

Please help improving Dune by testing the new build system. Report bugs, share success stories, and improve the documentation in the wiki!

Our plan is to keep Autotools for the Dune 2.4 release and drop it for Dune 3.0. Once we dropped Autotools support, we are able to make major changes as we don't have to consider Autotools compatibility anymore.
