+++
date = "2015-11-06"
title = "Dune-Fem 2.4 Released"
+++

The developers of Dune-Fem are pleased to announce the release of version 2.4.0 of their discretization module. From now on, the version number of a Dune-Fem release will match the compatible version number of the Dune core modules, i.e., this version is compatible with the 2.4 release series of the Dune core modules. Besides the usual bug fixes and compatibility changes, the major features added in this release are:

-   cmake support
-   bindings the LDL and SPQR solvers from [SuiteSparse](http://faculty.cse.tamu.edu/davis/suitesparse.html)
-   introduction of a (Local)DofVector concept
-   revision of GridParts based on GridViews
