+++
date = "2016-02-29"
title = "Dune 2.4.1 Released"
+++

We are pleased to announce the release of Dune 2.4.1, the first point release in the 2.4 release series of the Dune core modules. This release mostly contains bug fixes for problems in the original 2.4.0 release, but also a number of backported feature from the master development branch. For a list of the fixed bugs, new features and known issues take a look at the [release notes](/releases/2.4.1/). You can download the tarballs from our [download](/releases/) page or checkout the v2.4.1 tag of the modules via git. Please go and test, and report the problems that you encounter.
